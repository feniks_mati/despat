using UnityEngine;
using CnControls;

namespace UnityStandardAssets.Copy._2D
{
    [RequireComponent(typeof(PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {

        public int hp = 100;
        public int Hp
        {
            get
            {
                return hp;
            }
            set
            {
                hp = value;
                if (hp <= 0)
                {
                    hp = 0;
                    death = true;
                    anim.SetBool("Die", death);
                }

            }
        }
        private PlatformerCharacter2D m_Character;
        private bool move = false;
        private bool death = false;
        private Animator anim;
        private Vector2 lastMove;

        private const float rangeMove = 0.5f;
        private const int takenDamage = 10;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
            anim = GetComponent<Animator>();
        }

        private void FixedUpdate()
        {

            float horizontalAxis = CnInputManager.GetAxis("Horizontal");
            float verticalAxis = CnInputManager.GetAxis("Vertical");

            if (horizontalAxis == 0 && verticalAxis == 0)
            {
                move = false;
            }

            else if (horizontalAxis <= rangeMove && verticalAxis <= rangeMove && horizontalAxis >= -rangeMove && verticalAxis >= -rangeMove)
            {
                move = false;
                horizontalAxis = 0;
                verticalAxis = 0;
            }
            else
            {
                move = true;
                lastMove.x = CnInputManager.GetAxis("Horizontal");
                lastMove.y = CnInputManager.GetAxis("Vertical");
            }

            if (!death)
            {
                anim.SetFloat("InputX", horizontalAxis);
                anim.SetFloat("InputY", verticalAxis);
                anim.SetBool("Move", move);
                anim.SetFloat("LastX", lastMove.x);
                anim.SetFloat("LastY", lastMove.y);
                m_Character.Move(horizontalAxis, verticalAxis);
            }
        }

        public void GetDamage()
        {
            Hp -= takenDamage;
        }

        public bool GetDeath()
        {
            return death;
        }
    }
}
