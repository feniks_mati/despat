﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CreaturePattern
{
    public class Friend : Creature
    {

        public Friend(GameObject g, TypeCreature typeCreature, Animator a, GameObject e, AudioClip clip) : base(g.transform)
        {
            this.creatureTrans = g.transform;
            this.typeCreature = typeCreature;
            this.hp = 100;
            this.anim = a;
            anim.SetBool("Move", true);
            this.explosion = e;
            this.clip = clip;
        }

        protected override void Move(Creature target)
        {
            base.Move(target);
        }

        protected override void UpdateState(CreatureState creatureState)
        {
            base.UpdateState(creatureState);
        }

        protected override void Damage(TypeCreature typeEnemy)
        {
            GameObject g = Lean.LeanPool.Spawn(explosion, creatureTrans.transform.position, Quaternion.identity);
            Lean.LeanPool.Despawn(g, 2);
            switch (typeEnemy)
            {
                case TypeCreature.TypeOne:
                    switch (typeCreature)
                    {
                        case TypeCreature.TypeOne:
                            DamageMediumAttack();
                            break;

                        case TypeCreature.TypeTwo:
                            DamageStrongAttack();
                            break;

                        case TypeCreature.TypeThree:
                            DamageWeakAttack();
                            break;
                    }
                    break;

                case TypeCreature.TypeTwo:
                    switch (typeCreature)
                    {
                        case TypeCreature.TypeOne:
                            DamageWeakAttack();
                            break;

                        case TypeCreature.TypeTwo:
                            DamageMediumAttack();
                            break;

                        case TypeCreature.TypeThree:
                            DamageStrongAttack();
                            break;
                    }
                    break;

                case TypeCreature.TypeThree:
                    switch (typeCreature)
                    {
                        case TypeCreature.TypeOne:
                            DamageStrongAttack();
                            break;

                        case TypeCreature.TypeTwo:
                            DamageWeakAttack();
                            break;

                        case TypeCreature.TypeThree:
                            DamageMediumAttack();
                            break;
                    }
                    break;
            }
            CheckMyDeath();
        }
    }
}
