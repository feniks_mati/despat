﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioSource audioSource;

    public AudioClip hit;
    public AudioClip spider;
    public AudioClip[] monsters;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySpiderSound()
    {
        audioSource.PlayOneShot(spider, 1);
    }

    public void PlayMonsterSound()
    {
        int random = Random.Range(0, monsters.Length);
        audioSource.PlayOneShot(monsters[random]);
    }
}
