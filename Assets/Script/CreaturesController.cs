﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Creature = CreaturePattern.Creature;

public class CreaturesController : MonoBehaviour {

    public SpawnController spawnController;

    Creature closestObject = null;
    // Use this for initialization
 
    private void FixedUpdate()
    {
        closestObject = null;
        UpdateEnemiesBehaviour();
        UpdateFriendBehaviour();
    }

    void UpdateEnemiesBehaviour()
    {
        for (int i = 0; i < spawnController.listEnemies.Count; i++)
        {
            if (!spawnController.listEnemies[i].death)
            {
                SetNearestTargetForEnemy(i);
                spawnController.listEnemies[i].DoAction();
            }
            else
            {
                spawnController.DestroyEnemy(i);
            }
        }
    }

    void UpdateFriendBehaviour()
    {
        for (int i = 0; i < spawnController.listFriends.Count; i++)
        {
            if (!spawnController.listFriends[i].death)
            {
                closestObject = FindClosestObjectForFriend(spawnController.listFriends[i]);
                spawnController.listFriends[i].AddClosestObject(closestObject);
                spawnController.listFriends[i].DoAction();
            }
            else
            {
                spawnController.DestroyFriend(i);
            }
        }
    }

    private void SetNearestTargetForEnemy(int index)
    {
        closestObject = FindClosestObjectForEnemy(spawnController.listEnemies[index]);
        if (closestObject == null)
        {
            spawnController.listEnemies[index].AddClosestObject(spawnController.GetPlayerCreature());
        }
        else
        {
            float distance = Vector3.Distance(closestObject.creatureTrans.position, spawnController.listEnemies[index].creatureTrans.position);
            if (distance <= 0.8f)
            {
                spawnController.listEnemies[index].AddClosestObject(closestObject);
            }
            else
            {
                spawnController.listEnemies[index].AddClosestObject(spawnController.GetPlayerCreature());
            }
        }
    }

    Creature FindClosestObjectForEnemy(Creature creature)
    {
        Creature foundedCreature = null;

        float bestDistance = Mathf.Infinity;

        for (int i = 0; i < spawnController.listFriends.Count; i++)
        {
            float distance = (creature.creatureTrans.position - spawnController.listFriends[i].creatureTrans.position).sqrMagnitude;
            if (distance < bestDistance)
            {
                bestDistance = distance;
                foundedCreature = spawnController.listFriends[i];
            }
        }
        return foundedCreature;
    }

    Creature FindClosestObjectForFriend(Creature creature)
    {
        Creature foundedCreature = null;

        float bestDistance = Mathf.Infinity;

        for (int i = 0; i < spawnController.listEnemies.Count; i++)
        {
            float distance = (creature.creatureTrans.position - spawnController.listEnemies[i].creatureTrans.position).sqrMagnitude;
            if (distance < bestDistance)
            {
                bestDistance = distance;
                foundedCreature = spawnController.listEnemies[i];
            }
        }
        return foundedCreature;
    }
}
