﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    private const int nextScene = 1;
    public void StartGame()
    {
        SceneManager.LoadScene(nextScene);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
