﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CreaturePattern;

public class SpawnController : MonoBehaviour
{
    public AudioManager audioManager;

    public GameObject[] tabFriends;
    public GameObject[] tabEnemies;
    public GameObject[] blood;

    public Transform[] tabSpawn;

    public Transform friendParent;
    public Transform enemyParent;

    public GameObject explosion;

    public List<Creature> listFriends = new List<Creature>();
    public List<Creature> listEnemies = new List<Creature>();

    Creature playerCreature = new Creature();

    private float timer = 0.0f;
    private float timerRound = 5;
    private float timerSpawn = 0;
    private const int timeIncreaseSpeedSpawn = 30;

    private bool canSpawn = true;
    public bool CanSpawn
    {
        get
        {
            return canSpawn;
        }
        set
        {
            canSpawn = value;
        }
    }

    public void Initialize(Transform transformPlayer)
    {
        timer = 0.0f;
        playerCreature.creatureTrans = transformPlayer;
        CanSpawn = true;
    }

    public Creature GetPlayerCreature()
    {
        return playerCreature;
    }

    private void FixedUpdate()
    {
        UpdateTimeSpawn();
    }

    private void UpdateTimeSpawn()
    {
        timer += Time.fixedDeltaTime;
        timerSpawn += Time.fixedDeltaTime;

        if (timerSpawn >= timeIncreaseSpeedSpawn)
        {
            timerRound /= timerRound;
            timerSpawn = 0.0f;

        }
        if (timer >= timerRound && CanSpawn)
        {
            CreateEnemy();
        }
    }

    private void CreateEnemy()
    {
        audioManager.PlaySpiderSound();
        timer = 0.0f;
        int randomEnemy = Random.Range(0, tabEnemies.Length);
        int randomPosition = Random.Range(0, tabSpawn.Length);
        GameObject g = Lean.LeanPool.Spawn(tabEnemies[randomEnemy], new Vector2(tabSpawn[randomPosition].position.x, tabSpawn[randomPosition].position.y), Quaternion.identity);
        Animator animator = g.GetComponent<Animator>();
        listEnemies.Add(new Enemy(g, (Creature.TypeCreature)randomEnemy, animator, explosion, audioManager.hit));
        g.transform.parent = enemyParent;

    }

    public void DestroyEnemy(int indexEnemy)
    {
        GameObject destroyG = listEnemies[indexEnemy].creatureTrans.gameObject;
        listEnemies.RemoveAt(indexEnemy);
        int random = Random.Range(0, blood.Length);
        GameObject imageBlood = Lean.LeanPool.Spawn(blood[random], destroyG.transform.position, Quaternion.identity);
        Lean.LeanPool.Despawn(imageBlood, 10);
        Lean.LeanPool.Despawn(destroyG);
    }

    public void DestroyFriend(int indexFriend)
    {
        GameObject destroyG = listFriends[indexFriend].creatureTrans.gameObject;
        listFriends.RemoveAt(indexFriend);
        int random = Random.Range(0, 5);
        GameObject imageBlood = Lean.LeanPool.Spawn(blood[random], destroyG.transform.position, Quaternion.identity);
        Lean.LeanPool.Despawn(imageBlood, 10);
        Lean.LeanPool.Despawn(destroyG);
    }

    public void CreateFriend(int indexButton)
    {
        if (CanSpawn)
        {
            audioManager.PlayMonsterSound();
            GameObject g = Lean.LeanPool.Spawn(tabFriends[indexButton], new Vector2(playerCreature.creatureTrans.position.x, playerCreature.creatureTrans.position.y), Quaternion.identity);
            Animator animator = g.GetComponent<Animator>();
            listFriends.Add(new Friend(g, (Creature.TypeCreature)indexButton, animator, explosion, audioManager.hit));
            g.transform.parent = friendParent;
        }
    }
}
