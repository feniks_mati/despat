﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CreaturePattern
{

    public class Creature
    {
        public enum TypeCreature
        {
            TypeOne,
            TypeTwo,
            TypeThree
        }

        public Transform creatureTrans;
        public TypeCreature typeCreature;
        public bool death = false;
        public Animator anim;

        protected GameObject explosion;
        protected Creature closestObject;
        protected Rigidbody2D rigidbody;
        protected int hp;
        protected float damage;
        protected Vector2 direction;
        protected Vector2 LastMove;
        protected AudioClip clip;

        private const float rangeAttack = 0.8f;
        private const int weakAttack = 10;
        private const int mediumAttack = 20;
        private const int strongAttack = 50;

        public enum CreatureState
        {
            Move,
            Attack,
            Idle
        }

        public CreatureState state = CreatureState.Move;

        public Creature() { }
        protected virtual void Damage(TypeCreature type) { }

        public Creature(Transform t)
        {
            rigidbody = t.GetComponent<Rigidbody2D>();
        }

        public void AddClosestObject(Creature c)
        {
            closestObject = c;
        }

        protected virtual void UpdateState(CreatureState creatureState)
        {
            TryToChangeState(creatureState, CalculateDistanceToNearestObject());
        }

        private float CalculateDistanceToNearestObject()
        {
            if (closestObject != null)
            {
                return (closestObject.creatureTrans.position - creatureTrans.position).sqrMagnitude;
            }
            else
            {
                return 0.0f;
            }
        }

        void TryToChangeState(CreatureState creatureState, float distance)
        {
            switch (creatureState)
            {
                case CreatureState.Move:

                    if (closestObject == null)
                    {
                        ChangeToStateIdle();

                    }
                    else if (distance <= rangeAttack)
                    {
                        ChangeToStateAttack();
                    }

                    break;

                case CreatureState.Attack:
                    if (closestObject == null)
                    {
                        ChangeToStateIdle();
                    }
                    else if (distance > rangeAttack)
                    {
                        ChangeToStateMove();
                    }
                    break;

                case CreatureState.Idle:
                    if (closestObject != null)
                    {
                        if (distance <= rangeAttack)
                        {
                            ChangeToStateAttack();
                        }
                        else
                        {
                            ChangeToStateMove();
                        }
                    }

                    break;
            }
        }

        private void ChangeToStateIdle()
        {
            state = CreatureState.Idle;
            DisableMoveAnimation();
        }

        private void ChangeToStateAttack()
        {
            state = CreatureState.Attack;
            DisableMoveAnimation();
        }

        private void ChangeToStateMove()
        {
            state = CreatureState.Move;
            PlayMoveAnimation();
        }

        void PlayMoveAnimation()
        {
            anim.SetBool("Move", true);
        }

        void DisableMoveAnimation()
        {
            anim.SetBool("Move", false);
            anim.SetFloat("LastX", LastMove.x);
            anim.SetFloat("LastY", LastMove.y);
        }

        public virtual void DoAction()
        {
            UpdateState(state);

            switch (state)
            {
                case CreatureState.Move:
                    Move(closestObject);
                    break;
                case CreatureState.Attack:
                    Attack();
                    break;
                default:
                    break;
            }

        }
        protected virtual void Move(Creature c)
        {
            creatureTrans.position = Vector3.MoveTowards(creatureTrans.position, c.creatureTrans.position, Time.fixedDeltaTime);
            Vector3 heading = c.creatureTrans.position - creatureTrans.position;
            heading = heading / heading.magnitude;
            direction = new Vector2(heading.x * 10, heading.y * 10);
            anim.SetFloat("InputX", heading.x);
            anim.SetFloat("InputY", heading.y);
            LastMove.x = heading.x;
            LastMove.y = heading.y;
        }


        protected virtual void Attack()
        {

            AudioManager.audioSource.PlayOneShot(clip);

            rigidbody.AddForce((closestObject.creatureTrans.position - creatureTrans.position).normalized * -5, ForceMode2D.Impulse);
            if (closestObject.rigidbody != null)
            {
                closestObject.rigidbody.AddForce((creatureTrans.position - closestObject.creatureTrans.position).normalized * -5, ForceMode2D.Impulse);

                closestObject.Damage(this.typeCreature);
                Damage(closestObject.typeCreature);
            }
            else
            {
                closestObject.creatureTrans.gameObject.GetComponent<UnityStandardAssets.Copy._2D.Platformer2DUserControl>().GetDamage();

            }

        }

        protected virtual void DamageWeakAttack()
        {
            hp -= weakAttack;
        }
        protected virtual void DamageMediumAttack()
        {
            hp -= mediumAttack;
        }
        protected virtual void DamageStrongAttack()
        {
            hp -= strongAttack;
        }

        protected virtual void CheckMyDeath()
        {
            if (hp <= 0)
            {
                death = true;
            }
        }

    }
}
