﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Copy._2D;

namespace CreaturePattern
{
    public class GameController : MonoBehaviour
    {
        public SpawnController spawnController;

        public Text end;
        public Text life;

        public GameObject player;

        private Platformer2DUserControl playerOri;

        void Start()
        {
            InitializeGame();
        }

        void InitializeGame()
        {
            playerOri = player.GetComponent<Platformer2DUserControl>();
            spawnController.Initialize(player.transform);
        }

        private void Update()
        {
            if (spawnController.CanSpawn)
            {
                CheckEndGame();
                life.text = playerOri.Hp.ToString();
            }
        }

        private void CheckEndGame()
        {
            if (playerOri.GetDeath())
            {
                spawnController.CanSpawn = true;
                end.gameObject.SetActive(true);
            }
        }

        public void Exit()
        {
            SceneManager.LoadScene(0);
        }
    }


}
